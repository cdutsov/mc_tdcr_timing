use crate::mc_io::Config;
use crate::phys::{DelayedDecay, Photon};
use crate::Spectrum;
use rand::rngs::SmallRng;
use rand::SeedableRng;
use rand_distr::{Distribution, Exp, Normal, Poisson, Uniform, WeightedIndex};

pub struct Pmt {
    pub efficiency: f64,
    pub photons: Vec<Photon>,
}

impl Pmt {
    pub fn new(efficiency: f64) -> Pmt {
        Pmt {
            efficiency,
            photons: vec![],
        }
    }
}

pub struct Detector {
    pub pmts: [Pmt; 3],
    pub avg_photons: f64,
    pub avg_energy: f64,
    pub uniform: Uniform<f64>,
    pub weighted: WeightedIndex<f64>,
    pub fast_exp: Exp<f64>,
    pub radioactive_decay: Exp<f64>,
    pub slow_exp: Exp<f64>,
    pub delayed_comp: DelayedDecay,
    pub gauss: Normal<f64>,
    decay_time: f64,
    photons_buffer: Vec<Photon>,
    rng: rand::rngs::SmallRng,
}

impl Detector {
    pub fn new(config: &Config) -> Detector {
        let pmts = [
            Pmt::new(config.pmt_efficiencies[0]),
            Pmt::new(config.pmt_efficiencies[1]),
            Pmt::new(config.pmt_efficiencies[2]),
        ];
        let pmt_efficiencies = [
            pmts[0].efficiency * 1e3,
            pmts[1].efficiency * 1e3,
            pmts[2].efficiency * 1e3,
        ];
        let weighted = WeightedIndex::new(&pmt_efficiencies).unwrap();
        let uniform = Uniform::new(0.0, 1.0);
        let gauss = Normal::new(0.0, config.gauss_sigma).unwrap();
        let fast_exp = Exp::new(config.prompt_lambda).unwrap();
        let radioactive_decay = Exp::new(config.activity / 1e9).unwrap();
        let slow_exp = Exp::new(0.012).unwrap();
        let delayed_comp = DelayedDecay::new(config.slow_lambda);
        let rng = SmallRng::from_entropy();

        Detector {
            pmts,
            avg_photons: 0.0,
            avg_energy: 0.0,
            uniform,
            gauss,
            fast_exp,
            slow_exp,
            delayed_comp,
            weighted,
            decay_time: 0.0,
            photons_buffer: vec![],
            rng,
            radioactive_decay,
        }
    }
    pub fn clear_photons(&mut self) {
        for mut pmt in &mut self.pmts {
            pmt.photons = vec![];
        }
    }

    pub fn increment_decay_time(&mut self) {
        self.decay_time += self.radioactive_decay.sample(&mut self.rng);
    }

    pub fn print_photons(&mut self) {
        let mut photons: Vec<Photon> = (0..3).map(|pmt| self.first_in_pmt(pmt)).collect();
        photons.sort();
        for photon in photons.iter_mut() {
            if !f64::is_finite(photon.ts + self.decay_time) {
                continue;
            }
            let new_photon = Photon::new(photon.ts + self.decay_time, photon.id + 1);
            self.photons_buffer.push(new_photon);
            if self.photons_buffer.len() > 20 {
                self.photons_buffer
                    .sort_by_cached_key(|&ph| std::cmp::Reverse(ph));
                self.photons_buffer.pop().unwrap().print();
            }
        }
    }

    fn first_in_pmt(&mut self, ch_ref: usize) -> Photon {
        let empty_photon = Photon::new(0.0, -1);
        let jitter = self.gauss.sample(&mut self.rng);
        let photon = self.pmts[ch_ref]
            .photons
            .iter()
            .min()
            .unwrap_or(&empty_photon);
        Photon::new(photon.ts + jitter, photon.id)
    }

    pub fn time_between(&mut self, ch_ref: usize, ch_sec: usize) -> f64 {
        let ref_pmt = self.first_in_pmt(ch_ref).ts;
        let sec_pmt = self.first_in_pmt(ch_sec).ts;
        sec_pmt - ref_pmt
    }

    fn _time_to_first(&self, ch_ref: usize) -> f64 {
        let ref_pmt = match self.pmts[ch_ref].photons.first() {
            Some(p) => p,
            None => panic!("No first photon"),
        };
        ref_pmt.ts
    }

    pub fn has_double_coincidence(&self, ch_1: usize, ch_2: usize) -> bool {
        if self.pmts[ch_1].photons.len() == 0 || self.pmts[ch_2].photons.len() == 0 {
            return false;
        }
        true
    }

    fn _has_triple_coincidence(&self) -> bool {
        if self.pmts[0].photons.len() == 0
            || self.pmts[1].photons.len() == 0
            || self.pmts[2].photons.len() == 0
        {
            return false;
        }
        true
    }

    pub fn gen_photons(&mut self, energy: f64, spectrum: &Spectrum, config: &Config) {
        // Here we will store the generated photons
        self.clear_photons();
        self.increment_decay_time();

        let q_cdf_index = (energy / spectrum.max_energy * 10000.0 - 1.0) as usize;
        let &eff_energy = match spectrum.q_cdf.get(q_cdf_index) {
            Some(eng) if *eng > 0.0 => eng,
            Some(eng) if *eng <= 0.0 => &config.ion_pot, // panic!("Energy sampled is less than 0: {}", *eng),
            Some(_) => unreachable!("Error in spectrum sampling"),
            None => unreachable!("Error in spectrum; no such index"),
        };

        // Calculate average number of photons for this energy
        let avg_photons = eff_energy * config.fom;
        // let avg_photons = config.fom;
        let poisson = match Poisson::new(avg_photons) {
            Ok(p) => p,
            Err(e) => panic!("Problem with poisson in gen_photons: {:?}", e),
        };
        // Get the number of photons to sample from Poisson and fill the list
        let n_photons: u64 = poisson.sample(&mut self.rng) as u64;
        // let n_photons = avg_photons as i64;
        //

        for _i in 0..n_photons {
            let photon_ts = self.fast_exp.sample(&mut self.rng);
            // Select PMT using efficiencies as weights
            let pmt_index = self.weighted.sample(&mut self.rng);
            // Create photon with a given timestamp and pmt index
            let photon = Photon::new(photon_ts, pmt_index as i32);
            self.pmts[pmt_index].photons.push(photon);
        }

        let poisson = match Poisson::new(eff_energy * config.slow_fom) {
            Ok(p) => p,
            Err(e) => panic!("Problem with poisson2 in gen_photons: {:?}", e),
        };
        let n_delayed: u64 = poisson.sample(&mut self.rng) as u64;
        for _i in 0..n_delayed {
            let photon_ts = self.delayed_comp.sample(&mut self.rng);
            // Select PMT using efficiencies as weights
            let pmt_index = self.weighted.sample(&mut self.rng);
            // Create photon with a given timestamp and pmt index
            let photon = Photon::new(photon_ts, pmt_index as i32);
            self.pmts[pmt_index].photons.push(photon);
        }

        self.avg_energy += eff_energy / config.mc_runs as f64;
        self.avg_photons += (n_photons + n_delayed) as f64 / config.mc_runs as f64;
    }
}
