use histogram::Histogram;
use rand::rngs::SmallRng;
use rand::SeedableRng;
use rand_distr::{Distribution, WeightedAliasIndex};

mod mc_io;
use mc_io::{output_histogram, Config};

mod phys;
use phys::Spectrum;

mod detector;
use detector::Detector;

fn main() {
    let mut config = Config::init();
    Config::get_args(&mut config);
    let spectrum = match config.mono == 0.0 {
        true => Spectrum::new(&config),
        false => Spectrum::new_mono(&config),
    };
    let mut histogram = Histogram::configure()
        .max_value(config.histogram_max)
        .precision(8)
        .build()
        .unwrap();

    let mut rng = SmallRng::from_entropy();
    // let mut rng = thread_rng();
    // Init PMTs
    let detector = &mut Detector::new(&config);

    let mut weights: Vec<f64> = vec![];
    for item in &spectrum.spectrum {
        weights.push(item.frequency * 1e3);
    }
    let sample_energy = match WeightedAliasIndex::new(weights) {
        Ok(r) => r,
        Err(e) => panic!("Error in alias_method: {}", e),
    };

    // Helper constants
    let bin_s = config.bin_size as f64;
    for _i in 0..config.mc_runs {
        let spectrum_index_rnd = sample_energy.sample(&mut rng);
        let energy = spectrum.spectrum[spectrum_index_rnd].energy;
        // let energy = spectrum._rejection_spectrum();
        detector.gen_photons(energy, &spectrum, &config);
        if !config.output_hist {
            detector.print_photons();
        }

        // If we generated less than 2 photons there is no coincidence
        if !detector.has_double_coincidence(0, 1) {
            continue;
        };

        let offset = (histogram.buckets_total() / 2) as f64 + config.centroid * bin_s;
        let time_delta = (detector.time_between(0, 1)) * bin_s + offset;
        // let time_delta = (detector.time_to_first(0)) * bin_s + offset;
        match histogram.increment(time_delta.round() as u64) {
            Ok(_) => continue,
            Err(_e) => continue,
        }
    }

    if config.output_hist {
        output_histogram(histogram, &config);
    }
    if config.output_avg {
        println!(
            "Avg ph: {} Avg. energy: {}",
            detector.avg_photons, detector.avg_energy
        );
    }
}
