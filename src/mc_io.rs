use histogram::Histogram;
extern crate argparse;
use argparse::{ArgumentParser, List, Store, StoreFalse, StoreTrue};

pub struct Config {
    pub mc_runs: u64,
    pub spectrum_file: String,
    pub fom: f64,
    pub histogram_max: u64,
    pub prompt_lambda: f64,
    pub slow_lambda: f64,
    pub slow_fom: f64,
    pub gauss_sigma: f64,
    pub centroid: f64,
    pub z_over_a: f64,
    pub rho: f64,
    pub ion_pot: f64,
    pub kb: f64,
    pub bin_size: u64,
    pub output_hist: bool,
    pub output_avg: bool,
    pub analytical_hist: bool,
    pub pmt_efficiencies: Vec<f64>,
    pub activity: f64,
    pub mono: f64,
}

impl Config {
    pub fn init() -> Config {
        Config {
            mc_runs: 1_000_000,
            fom: 0.0,
            spectrum_file: "".to_string(),
            prompt_lambda: 0.0,
            slow_lambda: 0.0,
            centroid: 0.0,
            slow_fom: 0.000001,
            gauss_sigma: 0.0,
            z_over_a: 0.5476,
            rho: 0.98,
            ion_pot: 0.06457,
            kb: 0.010,
            activity: 1000.0,
            bin_size: 0,
            histogram_max: 0,
            output_hist: true,
            output_avg: false,
            analytical_hist: false,
            pmt_efficiencies: vec![0.3333, 0.3333, 0.3333],
            mono: 0.0,
        }
    }

    pub fn get_args(config: &mut Config) {
        let mut bin_size_ns: f64 = 1.0;
        let mut hist_size: u64 = 1000;
        let mut output_avg: bool = false;
        let mut output_hist: bool = true;
        let analytical_hist: bool = false;
        let mut pmt_efficiencies = vec![0.3333, 0.3333, 0.3333];
        {
            let mut ap = ArgumentParser::new();
            ap.set_description("MonteCarlo simulation of PMT events in TDCR");
            ap.refer(&mut config.mc_runs)
                .add_option(&["--runs"], Store, "Number of MC runs");
            ap.refer(&mut config.fom)
                .add_option(
                    &["--fom", "-f"],
                    Store,
                    "Figure of merit, photoelectrons per keV",
                )
                .required();
            ap.refer(&mut config.spectrum_file)
                .add_option(&["--spectrum", "-s"], Store, "Spectrum 2 columns file");
            ap.refer(&mut config.prompt_lambda)
                .add_option(&["--prompt", "-p"], Store, "Prompt lambda, 1/ns")
                .required();
            ap.refer(&mut config.slow_lambda).add_option(
                &["--delayed", "-d"],
                Store,
                "Delayed constant, 1/ns",
            );
            ap.refer(&mut config.slow_fom).add_option(
                &["--slow-fom", "-F"],
                Store,
                "Delayed component FOM",
            );
            ap.refer(&mut config.gauss_sigma)
                .add_option(&["--sigma", "-g"], Store, "Gaussian sigma, ns")
                .required();
            ap.refer(&mut config.centroid).add_option(
                &["--center", "-c"],
                Store,
                "Centroid position, ns",
            );
            ap.refer(&mut config.z_over_a)
                .add_option(&["--za"], Store, "Z/A ratio of the cocktail");
            ap.refer(&mut config.rho)
                .add_option(&["--rho"], Store, "Density of the cocktail");
            ap.refer(&mut config.kb)
                .add_option(&["--kb"], Store, "kB parameter of the cocktail");
            ap.refer(&mut config.activity)
                .add_option(&["--activity", "-a"], Store, "Activity, Bq");
            ap.refer(&mut bin_size_ns)
                .add_option(&["--bin-size", "-b"], Store, "Bin size, ns");
            ap.refer(&mut hist_size)
                .add_option(&["--hist-size"], Store, "Bin size, ns");
            ap.refer(&mut output_avg).add_option(
                &["--avg"],
                StoreTrue,
                "Output average spectrum info",
            );
            ap.refer(&mut output_hist).add_option(
                &["--no-hist"],
                StoreFalse,
                "Remove output histogram",
            );
            ap.refer(&mut pmt_efficiencies).add_option(
                &["--pmt-eff", "-E"],
                List,
                "Relative efficiencies of PMTs A, B and C",
            );
            ap.refer(&mut config.mono).add_option(
                &["--mono"],
                Store,
                "Monoenergetic electron energy, keV",
            );
            ap.parse_args_or_exit();
        }
        let bin_size: u64 = (1.0 / bin_size_ns) as u64;
        let histogram_max: u64 = hist_size * bin_size;
        config.bin_size = bin_size;
        config.histogram_max = histogram_max;
        config.output_hist = output_hist;
        config.output_avg = output_avg;
        config.analytical_hist = analytical_hist;
        config.pmt_efficiencies = pmt_efficiencies;
    }
}

pub fn output_histogram(histogram: Histogram, config: &Config) {
    let mut s = 0.0;
    let bin = config.bin_size as f64;
    let mid = histogram.buckets_total() / 2;
    for i in 0..histogram.buckets_total() {
        s += histogram.get(i).unwrap() as f64;
    }
    println!(
        "{:10} {:^10} {:^10} {:^10} {:^10} {:^10} {:^10} {:^10}",
        "# Time", "Prob", "Stdev", "Counts", "None", "CDF_norm", "CDF", "None",
    );
    for i in 0..histogram.buckets_total() {
        let counts = histogram.get(i).unwrap() as f64;
        let time_delta = (i as f64 - (histogram.buckets_total() - 1) as f64 / 2.0) / bin;
        let cdf: f64 = ((mid as u64)..i)
            .map(|j: u64| {
                (histogram.get(j).unwrap() + histogram.get(mid - (j - mid - 1)).unwrap()) as f64
            })
            .sum();
        println!(
            "{:<10.4} {:10.8} {:10.9} {:10} {:10.3} {:10.8} {:10.3} {:10.3}",
            time_delta,
            counts / s * bin,
            counts.sqrt() / s * bin,
            counts,
            0.0,
            cdf / s,
            cdf,
            0.0,
        );
        // println!(
        //     "{:8}\t{:.8}\t{:.9}",
        //     (i as f64 - (histogram.buckets_total() - 1) as f64 / 2.0) / bin,
        //     counts / s * bin,
        //     counts.sqrt() / s * bin,
        // );
    }
}
