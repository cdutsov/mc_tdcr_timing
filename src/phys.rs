use crate::mc_io::Config;
use quadrature::integrate;
use rand::distributions::*;
use rand::Rng;
use std::cmp::Ordering;
use std::fs::File;
use std::io;
use std::io::Read;

#[derive(Debug)]
pub struct SpectrumColumn {
    pub energy: f64,
    pub frequency: f64,
}

pub struct Spectrum {
    pub spectrum: Vec<SpectrumColumn>,
    pub max_energy: f64,
    pub q_cdf: Vec<f64>,
}

impl Spectrum {
    pub fn new(config: &Config) -> Spectrum {
        let spectrum_string = Spectrum::filename_to_string(config.spectrum_file.as_str()).unwrap();
        let words_by_line = Spectrum::words_by_line(&spectrum_string);
        let mut spectrum: Vec<SpectrumColumn> = Vec::new();
        let mut energy = 0.0;
        let mut prev_energy = 0.0;
        for line in words_by_line {
            energy = line[0].parse::<f64>().unwrap();
            let delta_energy = energy - prev_energy;
            prev_energy = energy;
            let spe_col = SpectrumColumn {
                energy,
                frequency: line[1].parse::<f64>().unwrap() * delta_energy,
            };
            spectrum.push(spe_col);
        }
        let q_cdf = Spectrum::q_cdf(config, energy);
        Spectrum {
            spectrum,
            max_energy: energy,
            q_cdf,
        }
    }

    pub fn new_mono(config: &Config) -> Spectrum {
        let spectrum: Vec<SpectrumColumn> = vec![SpectrumColumn {
            energy: config.mono,
            frequency: 1.0,
        }];
        let q_cdf = Spectrum::q_cdf(config, config.mono);
        Spectrum {
            spectrum,
            max_energy: config.mono,
            q_cdf,
        }
    }

    pub fn q_cdf(config: &Config, max_energy: f64) -> Vec<f64> {
        let mut cdf: Vec<f64> = Vec::new();
        let max_energy = max_energy;
        let phys = Physics::new(config);
        let birks = |x| phys.birks(x);
        for i in 1..=10000 {
            let energy = max_energy / 10000.0 * i as f64;
            // cdf.push(integral(birks, config, config.ion_pot..energy, 1000))
            cdf.push(integrate(birks, config.ion_pot, energy, 1e-4).integral)
        }
        cdf
    }

    pub fn _rejection_spectrum(&self) -> f64 {
        let between_spectrum = Uniform::from(0..self.spectrum.len());
        let mut rng = rand::thread_rng();
        let rand_x = between_spectrum.sample(&mut rng);
        let rand_y = rng.gen_range(0.0..0.15);
        if self.spectrum[rand_x].frequency > rand_y {
            // println!("hit: {} {} {}", self.spectrum[rand_x].energy, rand_y, self.spectrum[rand_x].frequency);
        } else {
            // println!("miss: {} {} {}", self.spectrum[rand_x].energy, rand_y, self.spectrum[rand_x].frequency);
            self._rejection_spectrum();
        }
        return self.spectrum[rand_x].energy;
    }

    pub fn filename_to_string(s: &str) -> io::Result<String> {
        let mut file = File::open(s)?;
        let mut s = String::new();
        file.read_to_string(&mut s)?;
        Ok(s)
    }

    pub fn words_by_line<'a>(s: &'a str) -> Vec<Vec<&'a str>> {
        s.lines()
            .map(|line| line.split_whitespace().collect())
            .collect()
    }
}

#[derive(Clone, Copy)]
pub struct Photon {
    pub ts: f64,
    pub id: i32,
}

impl Photon {
    pub fn new(ts: f64, id: i32) -> Photon {
        Photon { ts, id }
    }

    pub fn print(&self) {
        if self.id != 0 {
            println!(
                "{:1},{:.15},{:1},{:1},{:1}",
                0,
                self.ts / 1e9,
                0,
                self.id,
                self.id
            );
        }
    }
}

impl Ord for Photon {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.ts == other.ts {
            return Ordering::Equal;
        } else if self.ts < other.ts {
            return Ordering::Less;
        } else {
            return Ordering::Greater;
        }
    }
}

impl PartialOrd for Photon {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Eq for Photon {}

impl PartialEq for Photon {
    fn eq(&self, other: &Self) -> bool {
        self.ts == other.ts
    }
}

pub struct DelayedDecay {
    pub decay_constant: f64,
}

impl DelayedDecay {
    pub fn new(decay_constant: f64) -> DelayedDecay {
        DelayedDecay { decay_constant }
    }

    pub fn sample(&self, rng: &mut rand::rngs::SmallRng) -> f64 {
        let u = rng.gen_range(0.0..1.0) as f64;
        if u < 1e-5 {
            return 1e5;
        }
        ((1.0 - 2.0 * u).powf(-2.0) - 1.0) / self.decay_constant
        // let exp = Exp::new(self.decay_constant).unwrap();
        // exp.sample(rng)
    }
}

// struct _PromptDelayed {
//     prompt_c: f64,
//     delayed_c: f64,
//     weights: Vec<f64>,
//     weighted_alias: alias_method::WeightedIndex<f64>,
// }

// impl _PromptDelayed {
//     pub fn _new(prompt_c: f64, delayed_c: f64) -> _PromptDelayed {
//         let mut weights: Vec<f64> = Vec::new();
//         for i in 0..10_000 {
//             let pdf_slow = delayed_c / 40.0 / (1.0 + delayed_c * i as f64).powf(3.0 / 2.0);
//             let pdf_fast = prompt_c / 2.0 * f64::exp(-prompt_c * i as f64);
//             let pdf_tot = pdf_fast + pdf_slow;
//             weights.push(pdf_tot * 1e6);
//         }
//         let weights2 = weights.clone();
//         let weighted_alias = match alias_method::WeightedIndex::new(weights2) {
//             Ok(w) => w,
//             Err(e) => panic!("Problem with weights in PromptDelayed: {}", e),
//         };
//         _PromptDelayed {
//             prompt_c,
//             delayed_c,
//             weights,
//             weighted_alias,
//         }
//     }

// fn _sample(&self, rng: &mut rand::rngs::ThreadRng) -> f64 {
//     let index = self.weighted_alias.sample(rng);
//     index as f64
// }
// }

struct Physics {
    z_over_a: f64,
    ion_pot: f64,
    rho: f64,
    kb: f64,
}

impl Physics {
    fn new(config: &Config) -> Physics {
        Physics {
            z_over_a: config.z_over_a,
            ion_pot: config.ion_pot,
            rho: config.rho,
            kb: config.kb,
        }
    }
    pub fn linear_energy_transfer(&self, energy: f64) -> f64 {
        let mut eprim: f64 = 0.0;
        let mut e = energy;
        if energy < 0.1 {
            eprim = energy;
            e = 0.1;
        }

        let tau = e / 511.0;
        let beta2 = 1.0 - (511.0 / (e + 511.0)).powf(2.0);

        let effe = (1.0 - beta2) * (1.0 + tau * tau / 8.0 - (2.0 * tau + 1.0) * f64::ln(2.0));
        let mut tel = 0.153536 * self.z_over_a * self.rho / beta2;
        tel *= f64::ln((e / self.ion_pot).powf(2.0)) + f64::ln(1.0 + tau / 2.0) + effe;

        if e == 0.1 {
            tel *= eprim / 0.1
        }
        tel
    }

    pub fn birks(&self, energy: f64) -> f64 {
        1.0 / (1.0 + self.kb * self.linear_energy_transfer(energy))
        // f64::exp(-config.kb * linear_energy_transfer(energy, config))
    }
}

pub fn _integral<F>(f: F, config: &Config, range: std::ops::Range<f64>, n_steps: u32) -> f64
where
    F: Fn(f64, &Config) -> f64,
{
    if range.end < config.ion_pot {
        return 0.0000001;
    }
    let step_size = (range.end - range.start) / n_steps as f64;

    let mut integral = (f(range.start, config) + f(range.end, config)) / 2.;
    let mut pos = range.start + step_size;
    while pos < range.end {
        integral += f(pos, config);
        pos += step_size;
    }
    integral * step_size
}
